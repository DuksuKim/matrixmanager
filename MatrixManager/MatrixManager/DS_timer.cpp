#include "DS_timer.h"
#include "DS_definitions.h"
#include <iostream>

#ifndef _WIN32
#ifndef fprintf_s
#define fprintf_s fprintf
void fopen_s(FILE** fp, char* name, char* mode)
{
	*fp = fopen(name, mode);
}
#endif
#endif

/************************************************************************/
/* Constructor & Destructor                                             */
/************************************************************************/
DS_timer::DS_timer(int _numTimer /* =0 */, int _numCount /* =0 */, bool _trunOn /* =true */){

	turnOn = _trunOn ;
	start_ticks = NULL ; end_ticks = NULL ; totalTicks = NULL ;
	counters = NULL ;
	timerStates = NULL ;
	timerName = NULL ;
	counterName = NULL;

	numTimer = numCounter = 0 ;

	setTimerTitle("DS_timer Report") ;

	setNumTimer(_numTimer ) ;
	setNumCounter(_numCount);

#ifdef _WIN32
	// For windows
	QueryPerformanceFrequency(&ticksPerSecond) ;
#endif
}

DS_timer::~DS_timer(void){
	releaseTimers() ;
	releaseCounters() ;
}

/************************************************************************/
/* Set & Get configurations                                             */
/************************************************************************/

// Memory allocation
void DS_timer::memAllocCounters( void ) {
	releaseCounters() ;
	counters = new UINT[numCounter] ;

	counterName = new std::string[numCounter];
	for (UINT i = 0; i < numCounter; i++) {
		counterName[i].clear();
		counterName[i].resize(255);
	}

	initCounters() ;
}

void DS_timer::memAllocTimers( void ) {
	releaseTimers() ;

	timerStates = new bool[numTimer] ;
	start_ticks = new TIME_VAL[numTimer];
	end_ticks = new TIME_VAL[numTimer];
	totalTicks = new TIME_VAL[numTimer];

	// Initialize
	memset(timerStates, 0, sizeof(bool)*numTimer);
	memset(start_ticks, 0, sizeof(TIME_VAL)*numTimer);
	memset(end_ticks, 0, sizeof(TIME_VAL)*numTimer);
	memset(totalTicks, 0, sizeof(TIME_VAL)*numTimer);

	timerName = new std::string[numTimer] ;
	for ( UINT i = 0 ; i < numTimer ; i++ ) {
		timerName[i].clear() ;
		timerName[i].resize(255) ;
	}

	initTimers();
}

// Memory release
void DS_timer::releaseCounters(void) {
	SAFE_DELETE_ARR(counters);
	SAFE_DELETE_ARR(counterName);
}

void DS_timer::releaseTimers( void ) {
	SAFE_DELETE_ARR(timerStates);
	SAFE_DELETE_ARR(start_ticks);
	SAFE_DELETE_ARR(end_ticks);
	SAFE_DELETE_ARR(totalTicks);
	SAFE_DELETE_ARR(timerName);
}

// Getters
UINT DS_timer::getNumTimer( void ) { return numTimer ; }
UINT DS_timer::getNumCounter( void ) { return numCounter ; }

// Setters
UINT DS_timer::setNumTimer( UINT _numTimer ) {
	if ( _numTimer == 0 )
		return 0 ;

	if (_numTimer <= numTimer)
		return numTimer;

	if ( numTimer != 0 ) {

		// Backup
		UINT oldNumTimer = numTimer ;
		TIME_VAL *oldTotalTicks = new TIME_VAL[oldNumTimer];
		memcpy(oldTotalTicks, totalTicks, sizeof(TIME_VAL)*oldNumTimer);

		numTimer = _numTimer ;
		memAllocTimers() ;

		memcpy(totalTicks, oldTotalTicks, sizeof(TIME_VAL)* oldNumTimer);
		delete [] oldTotalTicks ;
	} else {
		numTimer = _numTimer ;
		memAllocTimers() ;
	}

	return _numTimer ;
}

UINT DS_timer::setNumCounter( UINT _numCounter ) {

	if (_numCounter == 0 )
		return 0 ;

	if (_numCounter <= numCounter)
		return numCounter;

	if ( numCounter != 0 ) {

		// Backup
		int numOldCounter = numCounter ;
		UINT *oldCounters = new UINT[numOldCounter] ;
		memcpy(oldCounters, counters, sizeof(UINT)*numOldCounter) ;

		numCounter = _numCounter ;
		memAllocCounters() ;

		// Restore
		memcpy(counters, oldCounters, sizeof(UINT)*numOldCounter) ;
		delete [] oldCounters ;

	} else {
		numCounter = _numCounter ;
		memAllocCounters() ;
	}

	return numCounter ;

}

/************************************************************************/
/* Timer                                                                */
/************************************************************************/
void DS_timer::initTimer( UINT id ) {
	timerStates[id] = TIMER_OFF ;
#ifdef _WIN32
	totalTicks[id].QuadPart = 0 ;
#else
	totalTicks[id].tv_sec = 0;
	totalTicks[id].tv_usec = 0;
#endif
}

void DS_timer::initTimers( void ) {
	for ( UINT i = 0 ; i < numTimer ; i++ ) {
		initTimer(i);
	}
}

void DS_timer::onTimer( UINT id ) {
	if ( turnOn == false )
		return ;

	if ( timerStates[id] == TIMER_ON )
		return ;
#ifdef _WIN32
	QueryPerformanceCounter(&start_ticks[id]) ;
#else
	gettimeofday(&start_ticks[id], NULL);
#endif

	timerStates[id] = TIMER_ON ;
}

void DS_timer::offTimer( UINT id ) {
	if ( turnOn == false )
		return ;

	if ( timerStates[id] == TIMER_OFF )
		return ;

#ifdef _WIN32
	QueryPerformanceCounter(&end_ticks[id]) ;
	totalTicks[id].QuadPart = totalTicks[id].QuadPart + (end_ticks[id].QuadPart - start_ticks[id].QuadPart) ;
#else
	gettimeofday(&end_ticks[id], NULL);
	TIME_VAL period, previous;
	timersub(&end_ticks[id], &start_ticks[id], &period);
	previous = totalTicks[id];
	timeradd(&previous, &period, &totalTicks[id]);
#endif

	timerStates[id] = TIMER_OFF ;
}

void DS_timer::setTimer(UINT id, TIME_VAL _tick)
{
	totalTicks[id] = _tick;
}

double DS_timer::getTimer_ms( UINT id ) {
#ifdef _WIN32
	return ((double)totalTicks[id].QuadPart/(double)ticksPerSecond.QuadPart * 1000) ;
#else
	return (double)(totalTicks[id].tv_sec * 1000 + totalTicks[id].tv_usec / 1000.0);
#endif
}

TIME_VAL DS_timer::getTimer_tick(UINT id)
{
#ifdef _WIN32
	return totalTicks[id];
#else
	return totalTicks[id];
#endif

}

void DS_timer::timerAdd(UINT id, TIME_VAL _time)
{
#ifdef _WIN32
	totalTicks[id].QuadPart = totalTicks[id].QuadPart + _time.QuadPart;
#else
	TIME_VAL previous = totalTicks[id];
	timeradd(&previous, &_time, &totalTicks[id]);
#endif
}

/************************************************************************/
/* Counter                                                              */
/************************************************************************/
void DS_timer::incCounter( UINT id ) {
	if ( turnOn == false )
		return ;

	counters[id]++ ;
}

void DS_timer::initCounters( void ) {
	if ( turnOn == false )
		return ;

	for ( UINT i = 0 ; i < numCounter ; i++ )
		counters[i] = 0 ;
}

void DS_timer::initCounter( UINT id ) {
	if ( turnOn == false )
		return ;

	counters[id] = 0 ;
}

void DS_timer::add2Counter( UINT id, UINT num ) {
	if ( turnOn == false )
		return ;

	counters[id] += num ;
}

UINT DS_timer::getCounter( UINT id ) {
	if ( turnOn == false )
		return 0;

	return counters[id] ;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
void DS_timer::printTimer( float _denominator){

	if ( turnOn == false )
		return ;

	//printf("\n*\t DS_timer Report \t*\n") ;
	printf("\n*\t %s \t*\n", timerTitle) ;
	printf("* The number of timer = %d, counter = %d\n", numTimer, numCounter ) ;
	printf("**** Timer report ****\n") ;

	for ( UINT i = 0 ; i < numTimer ; i++ ) {
		if ( getTimer_ms(i) == 0 )
			continue ;
		if ( timerName[i].c_str()[0] == 0 )
			printf("Timer %d : %.5f ms (%.5f ms)\n", i, getTimer_ms(i)/_denominator, getTimer_ms(i) ) ;
		else
			printf("%s : %.5f ms (%.5f ms)\n", timerName[i].c_str(), getTimer_ms(i)/_denominator, getTimer_ms(i) ) ;
	}

	printf("**** Counter report ****\n") ;
	for ( UINT i = 0 ; i < numCounter ;i++ ) {
		if ( counters[i] == 0 )
			continue ;
		if (counterName[i].c_str()[0] == 0)
			printf("Counter %d : %.3f (%d) \n",i, counters[i]/_denominator, counters[i] ) ;
		else
			printf("%s : %.3f (%d) \n", counterName[i].c_str(), counters[i] / _denominator, counters[i]);
	}

	printf("*\t End of the report \t*\n") ;
}

void DS_timer::printToFile	(char* fileName, int _id,
							bool _printAllTimer, bool _printAllCounter)
{
	if ( turnOn == false )
		return ;

	FILE *fp = 0;

	if ( fileName == NULL)
		fopen_s(&fp, "DS_timer_report.txt", "a") ;
	else {
		fopen_s(&fp, fileName, "a") ;
	}

	if ( _id >= 0 )
		fprintf_s(fp,"%d\t", _id) ;

	for ( UINT i = 0 ; i < numTimer ; i++ ) {
		if (getTimer_ms(i) == 0 && !_printAllTimer)
			continue ;
		fprintf_s(fp, "%.9f\t", getTimer_ms(i) ) ;
	}

	for ( UINT i = 0 ; i < numCounter ;i++ ) {
		if (counters[i] == 0 && !_printAllCounter)
			continue ;
		fprintf_s(fp, "%d\t", counters[i] ) ;
	}

	fprintf_s(fp, "\n") ;

	fclose(fp) ;
}

void DS_timer::printTimerNameToFile( char* fileName )
{
	if ( turnOn == false )
		return ;

	FILE *fp ;

	if ( fileName == NULL)
		fopen_s(&fp, "DS_timer_name.txt", "a") ;
	else {
		fopen_s(&fp, fileName, "a") ;
	}


	for ( UINT i = 0 ; i < numTimer ; i++ ) {
		if ( timerName[i].empty() )
			continue ;
		fprintf_s(fp, "%s\t", timerName[i].c_str() ) ;
	}
	fprintf_s(fp, "\n") ;
	fclose(fp) ;
}

// Static methods
double DS_timer::tick2ms(TIME_VAL _totalTick)
{
#ifdef _WIN32
	TIME_VAL	_ticksPerSecond;
	QueryPerformanceFrequency(&_ticksPerSecond);
	return ((double)_totalTick.QuadPart / (double)_ticksPerSecond.QuadPart * 1000);
#else
	return (double)(_totalTick.tv_sec * 1000 + _totalTick.tv_usec / 1000.0);
#endif
}

TIME_VAL DS_timer::ms2tick(double _ms)
{
	TIME_VAL tick;
	memset(&tick, 0, sizeof(TIME_VAL));
#ifdef _WIN32
	TIME_VAL	_ticksPerSecond;
	QueryPerformanceFrequency(&_ticksPerSecond);

	tick.QuadPart = _ms * (double)_ticksPerSecond.QuadPart / 1000;
#else
	tick.tv_sec = _ms / 1000;
	tick.tv_usec = (_ms % 1000) * 1000;
#endif
	return tick;
}

void DS_timer::timerAdd(UINT id, TIME_VAL _time, TIME_VAL* _dest)
{
#ifdef _WIN32
	_dest->QuadPart = _dest->QuadPart + _time.QuadPart;
#else
	TIME_VAL previous = totalTicks[id];
	timeradd(&previous, &_time, _dest);
#endif
}

TIME_VAL DS_timer::timerSum_tick(DS_timer* _subTimers, int _numTimers, int _timerID )
{
	TIME_VAL sum;
	memset(&sum, 0, sizeof(TIME_VAL));

	LOOP_I(_numTimers) {
		timerAdd(_timerID, _subTimers[i].totalTicks[_timerID], &sum);
	}
	return sum;
}

TIME_VAL DS_timer::timerAvg_tick(DS_timer* _subTimers, int _numTimers, int _timerID )
{
	TIME_VAL sum = timerSum_tick(_subTimers, _numTimers, _timerID);
#ifdef _WIN32
	sum.QuadPart = sum.QuadPart / _numTimers;
#else
	sum = ms2tick((tick2ms(sum)/_numTimers));
#endif
	return sum;
}

double DS_timer::timerSum_ms(DS_timer* _subTimers, int _numTimers, int _timerID)
{
	double sum = 0;
	LOOP_I(_numTimers){
		sum += _subTimers[i].getTimer_ms(_timerID);
	}
	return sum;
}

double DS_timer::timerAvg_ms(DS_timer* _subTimers, int _numTimers, int _timerID)
{
	return timerSum_ms(_subTimers, _numTimers, _timerID) / _numTimers;
}

long DS_timer::counterSum(DS_timer* _subTimers, int _numTimers, int _counterID)
{
	long sum = 0;
	LOOP_I(_numTimers){
		sum += _subTimers[i].getCounter(_counterID);
	}
	return sum;
}

double DS_timer::counterAvg(DS_timer* _subTimers, int _numTimers, int _counterID)
{
	return (double)counterSum(_subTimers, _numTimers, _counterID) / (double)_numTimers;
}