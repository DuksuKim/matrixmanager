#include <stdio.h>
#include "DS_timer.h"
#include "../../MatrixManager.h"

#define DO_TEST_ASCII
#define DO_TEST_BINARY
#define DO_ACCURACY_TEST

#define dType float
#define intType __int64

enum TIMER {
	READ_ASCII, READ_BIN
	, WRITE_ASCII, WRITE_BIN
	, CONVERT_A2B, CONVERT_B2A
	, GENREATE_A, GENREATE_B
	, END
};

#define fileNameAscii "newMat.txt"
#define fileNameBinary "newMat.bin"
#define fileNameConv "convMat"

intType row = 4096;
intType col = 4096;
intType matSize;

dType *mat;
DS_timer *timer;
MatrixManager<dType, intType> *matManager;

void convertTest(void);
void generateTest(void);
void readTest();
void writeTest();

void main(int argc, char** argv)
{
	if (argc > 2) {
		row = atoi(argv[1]);
		col = atoi(argv[2]);
	}
	matSize = row * col;

	matManager = new MatrixManager<dType, intType>(4);
	timer = new DS_timer(TIMER::END);
	timer->initTimers();

	timer->setTimerName(READ_ASCII, "Read Ascii file");
	timer->setTimerName(READ_BIN, "Read Binary file");
	timer->setTimerName(WRITE_ASCII, "Write Ascii file");
	timer->setTimerName(WRITE_BIN, "Write Binary file");
	timer->setTimerName(CONVERT_A2B, "Convert Ascii to Binary");
	timer->setTimerName(CONVERT_B2A, "Convert Binary to Ascii");
	timer->setTimerName(GENREATE_A, "Generate an Ascii matrix");
	timer->setTimerName(GENREATE_B, "Generate a Binary matrix");

	printf("** Test Start **\n");
	printf("  * Matrix size = %d by %d\n", (int)row, (int)col);
	if (std::is_floating_point<dType>::value)
		printf("  * Data types = %d byte(s) Floating-point\n", sizeof(dType));
	else
		printf("  * Data types = %d byte(s) Integer value\n", sizeof(dType));
	

	//  Performance test
	generateTest();
	readTest();
	writeTest();
	convertTest();
	
	timer->printTimer();

	// Accuracy test
#ifdef DO_ACCURACY_TEST
	printf("== Start accuracy test ==\n");
	dType *matRead, *matConv;
	SAFE_NEW(matRead, dType, matSize);
	SAFE_NEW(matConv, dType, matSize);

	printf("  * Read input matrix");
	matManager->readMatrix(fileNameAscii, __MM_MATRIX_FILETYPE::ASCII, &matRead, matSize, row, col);
	printf(" - done\n");

	printf("  * Convert");
	matManager->convertMatrix(fileNameAscii, fileNameConv, __MM_CONVERT_DIR::ASCII2BIN, row, col);
	printf(" - done\n");

	printf("  * Read output matrix");
	matManager->readMatrix(fileNameConv, __MM_MATRIX_FILETYPE::BINARY, &matConv, matSize, row, col);
	printf(" - done\n");

	bool isEqual = true;
	for (intType i = 0; i < matSize; i++) {
		if (matRead[i] != matConv[i]) {
			printf("[i] Resutls are not matched! - (%.1f, %.1f)\n", (float)matRead[i], (float)matConv[i]);
			isEqual = false;
		}
	}

	if (isEqual)
		printf("\n* Conversion result is correct :)\n");
	else
		printf("\n* Conversion result is not correct :(");

	SAFE_DELETE(matRead);
	SAFE_DELETE(matConv);
	printf("== End of accuracy test ==");
#endif
}

void convertTest(void)
{
	printf("== Start convert test ==\n");

#ifdef DO_TEST_ASCII
	//printf("start to conv ascii to binary\n");
	timer->onTimer(TIMER::CONVERT_A2B);
	matManager->convertMatrix(fileNameAscii, fileNameConv, __MM_CONVERT_DIR::ASCII2BIN, row, col);
	timer->offTimer(TIMER::CONVERT_A2B);
#endif

#ifdef DO_TEST_BINARY
	//printf("start to conv binary to ascii\n");
	timer->onTimer(TIMER::CONVERT_B2A);
	matManager->convertMatrix(fileNameBinary, fileNameConv, __MM_CONVERT_DIR::BIN2ASCII, row, col);
	timer->offTimer(TIMER::CONVERT_B2A);
#endif

	printf("== End convert test ==\n");
};

void generateTest(void)
{
	printf("== Start Generate test ==\n");

#ifdef DO_TEST_ASCII
	timer->onTimer(TIMER::GENREATE_A);
	matManager->genMatrix(fileNameAscii, __MM_MATRIX_FILETYPE::ASCII, row, col);
	timer->offTimer(TIMER::GENREATE_A);
#endif

#ifdef DO_TEST_BINARY
	timer->onTimer(TIMER::GENREATE_B);
	matManager->genMatrix(fileNameBinary, __MM_MATRIX_FILETYPE::BINARY, row, col);
	timer->offTimer(TIMER::GENREATE_B);
#endif

	printf("== End Generate test ==\n");
};

void readTest()
{
	printf("== Start read test ==\n");
	SAFE_NEW(mat, dType, matSize);

#ifdef DO_TEST_ASCII
	timer->onTimer(TIMER::READ_ASCII);
	matManager->readMatrix(fileNameAscii, __MM_MATRIX_FILETYPE::ASCII, &mat, matSize, row, col);
	timer->offTimer(TIMER::READ_ASCII);

	/*
	#ifdef _DEBUG
		for (intType irow = 0; irow < row; irow++) {
			for (intType icol = 0; icol < col; icol++) {
				printf("%.1f ", (float)mat[irow*row + icol]);
			}
			printf("\n");
		}
	#endif
	*/

#endif

#ifdef DO_TEST_BINARY
	timer->onTimer(TIMER::READ_BIN);
	matManager->readMatrix(fileNameBinary, __MM_MATRIX_FILETYPE::BINARY, &mat, matSize, row, col);
	timer->offTimer(TIMER::READ_BIN);

	/*
	#ifdef _DEBUG
		for (intType irow = 0; irow < row; irow++) {
			for (intType icol = 0; icol < col; icol++) {
				printf("%.1f ", (float)mat[irow*row + icol]);
			}
			printf("\n");
		}
	#endif
	*/
#endif

	SAFE_DELETE(mat);

	printf("== End reat test ==\n");
};

void writeTest()
{
#if defined(DO_TEST_ASCII) || defined(DO_TEST_BINARY)
	printf("== Start write test ==\n");
	mat = new dType[matSize];
	matManager->readMatrix(fileNameBinary, __MM_MATRIX_FILETYPE::BINARY, &mat, matSize, row, col);
#endif

#ifdef DO_TEST_ASCII
	timer->onTimer(TIMER::WRITE_ASCII);
	matManager->writeMatrix(fileNameAscii, __MM_MATRIX_FILETYPE::ASCII, mat, matSize);
	timer->offTimer(TIMER::WRITE_ASCII);
#endif

#ifdef DO_TEST_BINARY
	timer->onTimer(TIMER::WRITE_BIN);
	matManager->writeMatrix(fileNameBinary, __MM_MATRIX_FILETYPE::BINARY, mat, matSize);
	timer->offTimer(TIMER::WRITE_BIN);
#endif

#if defined(DO_TEST_ASCII) || defined(DO_TEST_BINARY)
	delete mat;
#endif
	printf("== End write test ==\n");
};