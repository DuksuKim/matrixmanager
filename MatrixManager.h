#pragma once

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <type_traits>
#include <time.h>
#include <omp.h>

enum __MM_MATRIX_FILETYPE {
	ASCII = 0, BINARY = 1
};

enum __MM_ERR {
	FILE_OPEN_FAIL = -1, NOT_ENOUGH_MEMORY = -2
};

enum __MM_CONVERT_DIR {
	ASCII2ASCII	= ((ASCII << 4) | ASCII),
	ASCII2BIN	= ((ASCII << 4) | BINARY),
	BIN2ASCII	= ((BINARY << 4) | ASCII),
	BIN2BIN		= ((BINARY << 4) | BINARY)
};

#define __MM_NAME "MatrixManager"

/**
Matrix manager libary <br>
This libary works in row-major manner.<br>
(TODO) support the column-major matrix

o Features<br>
  - Read matrxi from a file<br> 
  - Convert matrix to another types<br>
  - Generate a matrix (TODO)<br>
o Supported file type
  - Ascii
  - Binary

@Author Duksu Kim (bluekdct@gmail.com), HPC Lab., KOREATECH
*/
template<class T, class intType>
class MatrixManager
{
private: // interal variables
	FILE *fp;
	char fileName[255];
	T* mat;
	intType lenRow, lenCol;
	intType matrixSize;

	// for parallel computing
	int numThreads;

private:
	FILE* openFile(char* _fileName, int _type, char _mode = 'r');
	void closeFile(FILE* _fp) { if (_fp != NULL) fclose(_fp); }
	void closeFile() { closeFile(fp); }

	intType getMatrixSize(FILE *_fp = NULL);
	bool setMatrixSize(intType _lenRow, intType _lenCol);

	intType readBinaryMatrix();
	intType readAsciiMatrix();

	intType writeBinaryMatrix();
	intType writeAsciiMatrix();

	intType converBinaryToAscii() {};
	intType convertAsciiToBinary() {};

public:
	MatrixManager(int _numThreads = 1) {
		// Initailize for internal variables
		fp = NULL;
		mat = NULL;
		lenRow = lenCol = matrixSize = 0;
		memset(fileName, 0, sizeof(char) * 255);
		numThreads = _numThreads;
	};

	~MatrixManager() {
		// Release all resources 
		closeFile(fp);
	};

	/**
	Generate a new matrix
	@param _fileName	The file name to write the genernated matrix
	@param _type		Binary or Ascii (see the enum MATRIX_FILETYPE)
	@param _rowSize		Length of row
	@param _colSize		Length of column
	@param _rangeMin	The mininum value for a element (defualt: 0)
	@param _rangeMax	The maximum value for a element (defualt: 100)
	@param _zeroRatio	The ratio of zeros in the matrix (default: -1, totally random)
	@return				The size of the generated matrix
	*/
	intType genMatrix(char* _fileName, int _type, intType _rowSize, intType _colSize, int _rangeMin = 0, int _rangeMax = 100, float _zeroRatio = -1);
	
	/**
	Read a matrxi from a file
	@param _fileName	The file name to read
	@param _type		Binary or Ascii (see the enum MATRIX_FILETYPE)
	@param _dst			The pointer for storing the matrix (if NULL, it allocates memory space)
						<br> (Note: the caller should manage the memory space (e.g., release it)
	@param _dscCount	The _dst size in the number of elements 
	@param _lenRow		the length of row (default = 0 for a square matrix)
	@param _lenCol		the length of col (default = 0 for a square matrix)
	@return				The number of elements actually read (negative : error, see the __MM_ERR)
	*/
	intType readMatrix(char* _fileName, int _type, T** _dst, intType _dscCount, intType _lenRow = 0, intType _lenCol = 0);
	
	/**
	Write a matrxi to a file
	@param _fileName	The file name to write
	@param _type		Binary or Ascii (see the enum MATRIX_FILETYPE)
	@param _mat			Matrix to write
	@param _matCount	The number of elements in the matrix
	@return				The number of elements actually write (negative : error, see the enum __MM_ERR)
	*/
	intType writeMatrix(char* _fileName, int _type, T* _mat, intType _matCount);

	/**
	Interconversion between an ascii matrix and a binaray matrix
	@param	_src		The source file
	@param	_dst		The destination file
	@param	_converDir	Coversion direction (see the enum __MM_CONVERT_DIR)
	@param _lenRow		the length of row (default = 0 for a square matrix)
	@param _lenCol		the length of col (default = 0 for a square matrix)
	@return				The number of elements actually conveted
	*/
	intType convertMatrix(char* _src, char* _dst, int _convertDir, intType _lenRow = 0, intType _lenCol = 0);

	//// Utility methods
	void printErrMsg(int _errCode) {
		// TODO: implenet it
	};
};

//////////////////////////////////////
// Macro functions					//
//////////////////////////////////////

#ifndef SAFE_DELETE
#define	SAFE_DELETE(p) {if(p!=NULL) delete p; p=NULL;}
#endif

#ifndef SAFE_NEW
#define SAFE_NEW(p, type, size) {\
	try {p = new type[size];}	\
	catch(std::bad_alloc& exc) \
	{ printf("[%s, line %d] fail to memory allocation - %.2f MB requested\n", __FILE__, __LINE__, (float)(sizeof(type)*size)/(1024*1024));	\
	getchar(); exit(-1); }\
	}
#endif

//////////////////////////////////////
// Defintions for private Methods	//
//////////////////////////////////////

template<class T, class intType>
FILE* MatrixManager<T,intType>::openFile(char* _fileName, int _type, char _mode)
{
	char openMode[3] = { 0 };
	openMode[0] = _mode;

	strcpy_s(fileName, _fileName);

#ifdef _DEBUG
	printf("[%s] Target file : %s\n", __MM_NAME, fileName);
#endif

	switch (_type) {
	case __MM_MATRIX_FILETYPE::ASCII:
		fopen_s(&fp, _fileName, openMode);
		break;
	case __MM_MATRIX_FILETYPE::BINARY:
		openMode[1] = 'b';
		fopen_s(&fp, _fileName, openMode);
		break;
	default:
		printf("[%s] Unknown matrix types - %d\n", __MM_NAME, _type);
		break;
	}

	if (fp == NULL) {
		printf("[%s] file to open %s\n", __MM_NAME, fileName);
	}

	return fp;
}

template<class T, class intType>
intType MatrixManager<T, intType>::getMatrixSize(FILE *_fp) {
	if (_fp == NULL)
		return 0;

	fseek(_fp, 0, SEEK_END);
	long fileSize = ftell(_fp);
	fseek(_fp, 0, SEEK_SET);

	double numElelments = fileSize / sizeof(T);
#ifdef _DEBUG
	printf("[%s] file size = %ld, size of a element = %d -> # of ele = %ld\n"
		, __MM_NAME, fileSize, sizeof(T), numElelments);
	printf("[%s] Automatically compute the size..", __MM_NAME);
#endif

	return numElelments;
}

template<class T, class intType>
bool MatrixManager<T, intType>::setMatrixSize(intType _lenRow, intType _lenCol)
{
	// There is no input file
	if (fp == NULL) {
		lenRow = lenCol = 0;
		return false;
	}

	// Adjust a negative value to zero
	(_lenRow < 0 ? lenRow = 0 : lenRow = _lenRow);
	(_lenCol < 0 ? lenCol = 0 : lenCol = _lenCol);

	if (lenRow + lenCol == 0) { // Assum a square matrix
		matrixSize = getMatrixSize(fp); // This can be differnt with row * col in the case of non-square matrix
		lenRow = lenCol = sqrt(matrixSize);
	}
	else {
		matrixSize = lenRow * lenCol;
	}

	#ifdef _DEBUG
	printf("[%s] Matix = %d by %d \n", __MM_NAME, (unsigned int)lenRow, (unsigned int)lenCol);
	#endif
	
	return (matrixSize > 0 ? true : false);
}

#define readAsciiTemplate(_readType) { 					\
	for (count = 0; count < matrixSize; count++) {		\
		if (fscanf_s(fp, _readType, &mat[count]) == 0)		\
			break;										\
	}													\
}

template<class T, class intType>
intType MatrixManager<T, intType>::readAsciiMatrix()
{
	if (fp == NULL || mat == NULL)
		return 0;

	intType count = 0 ;
	if (std::is_floating_point<T>::value) {
		readAsciiTemplate("%f");
	}
	else {
		readAsciiTemplate("%d");
	}

	return count;
}

template<class T, class intType>
intType MatrixManager<T, intType>::readBinaryMatrix()
{
	if (fp == NULL || mat == NULL)
		return 0;

	intType count
		= fread_s(mat, matrixSize*sizeof(T), sizeof(T), matrixSize, fp);

	return count;
}

#define writeAsciiTemplate(_writeType) { 			\
	for (count = 0; count < matrixSize; count++) {		\
		fprintf_s(fp, _writeType, mat[count]);			\
	}													\
}

template<class T, class intType>
intType MatrixManager<T, intType>::writeAsciiMatrix()
{
	if (fp == NULL || mat == NULL)
		return 0;

	intType count = 0;
	if (std::is_floating_point<T>::value) {
		writeAsciiTemplate("%f ");
	}
	else {
		writeAsciiTemplate("%d ");
	}

	return count;
}

template<class T, class intType>
intType MatrixManager<T, intType>::writeBinaryMatrix()
{
	if (fp == NULL || mat == NULL)
		return 0;

	fwrite(mat, sizeof(T), matrixSize, fp);

	return matrixSize;
}

//////////////////////////////////////
// Defintions for public Methods	//
//////////////////////////////////////
template<class T, class intType>
intType MatrixManager<T, intType>::readMatrix
	(char* _fileName, int _type, T** _dst, intType _dscCount, intType _lenRow, intType _lenCol)
{
	if (openFile(_fileName, _type) == NULL)
		return __MM_ERR::FILE_OPEN_FAIL;

	if (!setMatrixSize(_lenRow, _lenCol)) {
		// Nothing to read
		closeFile();
		return 0;
	}

	// If _dst == NULL, it allocate memory
	if (*_dst == NULL) {
		SAFE_NEW(*_dst, T, matrixSize);
		_dscCount = matrixSize;
	}

	if (matrixSize > _dscCount) { // Not enough memory space on _dst
		closeFile();
		return __MM_ERR::NOT_ENOUGH_MEMORY;
	}

	mat = *_dst;

	intType readCount = 0;
	switch (_type) {
	case __MM_MATRIX_FILETYPE::ASCII:
		readCount = readAsciiMatrix();
		break;
	case __MM_MATRIX_FILETYPE::BINARY:
		readCount = readBinaryMatrix();
		break;
	default:
		printf("[%s] Unknown matrix types - %d\n", __MM_NAME, _type);
		break;
	}

	closeFile();
	return readCount;
}

template<class T, class intType>
intType MatrixManager<T, intType>::writeMatrix(char* _fileName, int _type, T* _mat, intType _matCount)
{
	if (_mat == NULL)
		return 0;

	if (openFile(_fileName, _type, 'w') == NULL)
		return __MM_ERR::FILE_OPEN_FAIL;

	mat = _mat;
	matrixSize = _matCount;

	intType readCount = 0;
	switch (_type) {
	case __MM_MATRIX_FILETYPE::ASCII:
		readCount = writeAsciiMatrix();
		break;
	case __MM_MATRIX_FILETYPE::BINARY:
		readCount = writeBinaryMatrix();
		break;
	default:
		printf("[%s] Unknown matrix types - %d\n", __MM_NAME, _type);
		break;
	}

	closeFile();
	return readCount;
}

template<class T, class intType>
intType MatrixManager<T, intType>::convertMatrix
	(char* _src, char* _dst, int _convertDir, intType _lenRow, intType _lenCol)
{
	int srcType = 0, dstType = 0;

	srcType = _convertDir >> 4;
	dstType = _convertDir & 0x0F;

	// read source matrix
	T* srcMat = NULL;
	intType readCount =
		readMatrix(_src, srcType, &srcMat, matrixSize, _lenRow, _lenCol);

	// write to destination file
	writeMatrix(_dst, dstType, srcMat, readCount);

	SAFE_DELETE(srcMat);

	return readCount;
}

template<class T, class intType>
intType MatrixManager<T, intType>::genMatrix
	(char* _fileName, int _type, intType _rowSize, intType _colSize, int _rangeMin, int _rangeMax, float _zeroRatio)
{
	lenRow = _rowSize; lenCol = _colSize;
	matrixSize = lenRow * lenCol;

	if (_rangeMax < _rangeMin) {
		printf("[%s] invalid range %f ~ %f \n", __MM_NAME, (float)_rangeMin, (float)_rangeMax);
		return 0;
	}

	int randMax = (int)(_rangeMax + abs(_rangeMin));

	// generate matrix
	SAFE_DELETE(mat);
	SAFE_NEW(mat, T, matrixSize);

	// fill with non-zero elements
	#pragma omp parallel num_threads(numThreads)
	{
		srand(time(NULL) ^ omp_get_thread_num());
		T val = 0;

		#pragma omp for
		for (intType i = 0; i < matrixSize; i++) {
				
			while ((val = rand() % randMax) == 0);

			if (std::is_floating_point<T>::value) {
				mat[i] = val + (rand() % 1000) / 1000.0;
			}
			else {
				mat[i] = val;
			}
			mat[i] -= _rangeMin;
		}
	}

	if (_zeroRatio > 0) {
		intType numZero = (intType)(matrixSize * _zeroRatio);
		intType zRow = 0, zCol = 0;
		for (intType i = 0; i < numZero; i++) {
			zRow = rand() % _rowSize;
			zCol = rand() % _colSize;
			mat[zRow * _rowSize + zCol] = 0;
		}
	}

	// write matrix
	writeMatrix(_fileName, _type, mat, matrixSize);

	SAFE_DELETE(mat);
	return matrixSize;
}