Matrix Manager library
=====================

* Basic usage
    * Check comments in the 'MatrixManager.h'
    * See the testCode in the VS project

---

## Features
  #### Read a matrix

```c
/**
Read a matrxi from a file
@param _fileName	The file name to read
@param _type		Binary or Ascii (see the enum MATRIX_FILETYPE)
@param _dst		The pointer for storing the matrix (if NULL, it allocates memory space) (Note: the caller should manage the memory space (e.g., release it)
@param _dscCount	The _dst size in the number of elements 
@param _lenRow		the length of row (default = 0 for a square matrix)
@param _lenCol		the length of col (default = 0 for a square matrix)
@return			The number of elements actually read (negative : error, see the __MM_ERR)
*/
intType readMatrix(char* _fileName, int _type, T** _dst, intType _dscCount, intType _lenRow = 0, intType _lenCol = 0)
```

  #### write a matrix

```c
/**
Write a matrxi to a file
@param _fileName	The file name to write
@param _type		Binary or Ascii (see the enum MATRIX_FILETYPE)
@param _mat		Matrix to write
@param _matCount	The number of elements in the matrix
@return			The number of elements actually write (negative : error, see the enum __MM_ERR)
*/
intType writeMatrix(char* _fileName, int _type, T* _mat, intType _matCount);
```

  #### Interconversion between an ascii and a binaray matrix files

```c
/**
Interconversion between an ascii matrix and a binaray matrix
@param	_src		The source file
@param	_dst		The destination file
@param	_converDir	Coversion direction (see the enum __MM_CONVERT_DIR)
@param _lenRow		the length of row (default = 0 for a square matrix)
@param _lenCol		the length of col (default = 0 for a square matrix)
@return			The number of elements actually conveted
*/
intType convertMatrix(char* _src, char* _dst, int _convertDir, intType _lenRow = 0, intType _lenCol = 0);
```

  #### Generate a new matrix
```c
/**
Generate a new matrix
@param _fileName	The file name to write the genernated matrix
@param _type		Binary or Ascii (see the enum MATRIX_FILETYPE)
@param _rowSize		Length of row
@param _colSize		Length of column
@param _rangeMin	The mininum value for a element (defualt: 0)
@param _rangeMax	The maximum value for a element (defualt: 100)
@param _zeroRatio	The ratio of zeros in the matrix (default: -1, totally random)
@return			The size of the generated matrix
*/
intType genMatrix(char* _fileName, int _type, intType _rowSize, intType _colSize, int _rangeMin = 0, int _rangeMax = 100, float _zeroRatio = -1);
```

----
### Test results

* Testing Envrionment 1

  |CPU | Memory | Disk |
  |--- |--- |--- |
  | Intel i7-8700 | 32GB DDR4 | WD SSD 512GB (blue) |

  |Matrix size (float) | Read (Ascii) | Read (Bin) | Write (Ascii) | Write (Bin)   |
  |:---:       | :---:        | :---:      | :---:         | :---:         |
  |4096 by 4096 | 4.7 sec | 0.01 sec | 5.7 sec | 0.15 sec |
  |8192 by 8192 | 18.5 sec | 0.06 sec | 22.3 sec | 0.7 sec |
  |16384 by 16384 | 73.7 sec | 0.2 sec | 88.9 sec | 2.9 sec |
  |36786 by 32786 | 295.9 sec | 1.0 sec | 355.6 sec | 11.8 sec |
  |65536 by 32768 | 586.3 sec | 18.2 sec | 708.1 sec | 22.3 sec |
  |65536 by 65536 | 1175.8 sec | 37.0 sec | - | - |


* Testing Envrionment 2
    * Intel i7 8550U CPU, 16 GB DDR4
    * Disk : M2 SSD
![Test result](./figs/MM_test.jpg)

